﻿using System;
using System.Collections.Generic;
using CompTheory.Graphs;
using CompTheory.LinearAlgebra;
using NUnit.Framework;

namespace CompTheory.Test.Graphs
{
	[TestFixture]
	public class ConvexHullTests
	{
		[Test]
		public void Test1()
		{
			var input = new List<Vector2<int>> {
				new Vector2<int> { X = 1, Y = 10 },
				new Vector2<int> { X = 6, Y = 2 },
				new Vector2<int> { X = 8, Y = 7 },
				new Vector2<int> { X = 2, Y = 3 },
				new Vector2<int> { X = 9, Y = 9 },
				new Vector2<int> { X = 6, Y = 6 }
			};
			var c = new ConvexHull {
				Input = input
			};
		}
	}
}
