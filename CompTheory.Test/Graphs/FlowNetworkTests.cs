using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace CompTheory.Test {
	[TestFixture]
	public class FlowNetworkTests {

		[Test]
		public void test1() {
			var vs = new FFNode<string> ("s");
			var va = new FFNode<string> ("a");
			var vb = new FFNode<string> ("b");
			var vc = new FFNode<string> ("c");
			var vd = new FFNode<string> ("d");
			var vt = new FFNode<string> ("t");

			new HashSet<int> ();

			var vertices = new HashSet<FFNode<string>> {
				vs,
				va,
				vb,
				vc,
				vd,
				vt
			};
			var edges = new HashSet<FFEdge<string>> {
				new FFEdge<string>(vs, va, 3),
				new FFEdge<string>(vs, vb, 3),
				new FFEdge<string>(va, vb, 2),
				new FFEdge<string>(va, vc, 3),
				new FFEdge<string>(vb, vd, 2),
				new FFEdge<string>(vc, vd, 4),
				new FFEdge<string>(vc, vt, 2),
				new FFEdge<string>(vd, vt, 3)
			};

			//Func<FFEdge<string>, string> printEdge = (edge) => {
			//	return edge.From.getValue() + " >>-- " + edge.mincap + "/" + edge.flow + "/" + edge.maxcap + " -->> " + edge.To.getValue();
			//};

			var g = new FlowNetwork<string> (edges, vertices, vs, vt);
			//Console.WriteLine(g.print());
			//Console.WriteLine(g.getExpandedNetwork().print());

			var maxFlow = new MaxFlow<string> (g.getExpandedNetwork());
			//maxFlow.flowEdges.Keys.ToList ().ForEach (node => {
			//	Console.WriteLine(node.getValue() + " = " + printEdge(maxFlow.flowEdges[node]));
			//});

			Console.WriteLine ("Output=");
			Console.WriteLine(maxFlow.graph.print());
		}
	}
}

