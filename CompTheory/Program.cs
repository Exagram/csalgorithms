﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompTheory.DynamicProgramming.Knapsack;
using CompTheory.Graphs;
using CompTheory.LinearAlgebra;

namespace CompTheory {
	class MainClass {
		public static void Main(string[] args)
		{
			var unboundedKnapsack = new BoundedKnapsackTopDown {
				Items = new List<ZeroOneItem> {
					new ZeroOneItem { Value = 160, Weight = 7 },
					new ZeroOneItem { Value = 90, Weight = 3 },
					new ZeroOneItem { Value = 15, Weight = 2 },
				},
				MaxWeight = 20
			};
			Console.WriteLine("=" + unboundedKnapsack.Solve());
			Console.ReadKey();
		}

		public static void MainOLD3(string[] args)
		{
			var input = new List<Vector2<int>> {
				new Vector2<int> { X = 1, Y = 1 },
				new Vector2<int> { X = 3, Y = 1 },
				new Vector2<int> { X = 5, Y = 1 },
				new Vector2<int> { X = 5, Y = 3 },
				new Vector2<int> { X = 5, Y = 5 },
				new Vector2<int> { X = 3, Y = 5 },
				new Vector2<int> { X = 1, Y = 5 },
				new Vector2<int> { X = 1, Y = 3 }
			};
			var c = new ConvexHull {
				Input = input
			};
			c.Run();
			Console.ReadKey();
		}

		public void MainOLD(string[] args) {

			Func<ISet<DijkstraNode<string>>, string, DijkstraNode<string>> setup = (nodes, key) => {
				var node = new DijkstraNode<string> (key);
				nodes.Add(node);
				return node;
			};

			var vertices = new HashSet<DijkstraNode<string>> { };

			var va = setup(vertices, "a");
			var vb = setup(vertices, "b");
			var vc = setup(vertices, "c");
			var vd = setup(vertices, "d");
			var ve = setup(vertices, "e");
			var vf = setup(vertices, "f");
			var vg = setup(vertices, "g");

			var edges = new HashSet<DijkstraEdge<string>> {
				new DijkstraEdge<string>(va, vb, 4),
				new DijkstraEdge<string>(va, vc, 3),
				new DijkstraEdge<string>(va, vd, 7),
				new DijkstraEdge<string>(vb, vd, 1),
				new DijkstraEdge<string>(vb, vf, 4),
				new DijkstraEdge<string>(vc, ve, 5),
				new DijkstraEdge<string>(vd, ve, 2),
				new DijkstraEdge<string>(vd, vf, 2),
				new DijkstraEdge<string>(vd, vg, 7),
				new DijkstraEdge<string>(vf, vg, 4),
				new DijkstraEdge<string>(ve, vg, 2)
			};

			//Func<FFEdge<string>, string> printEdge = (edge) => {
			//	return edge.From.getValue() + " >>-- " + edge.mincap + "/" + edge.flow + "/" + edge.maxcap + " -->> " + edge.To.getValue();
			//};

			var g = new DijkstraGraph<string>(vertices, edges);
			g.shortestPath(vertices.ElementAt(0), vertices.ElementAt(vertices.Count - 1)).ToList().ForEach(x => Console.WriteLine(x.getValue()));

			Console.ReadLine ();
			//g.bfs ();
		}
	}
}
