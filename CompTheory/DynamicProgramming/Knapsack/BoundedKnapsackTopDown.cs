﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompTheory.DynamicProgramming.Knapsack
{
	public class BoundedKnapsackTopDown
	{
		public List<ZeroOneItem> Items { get; set; }

		public int MaxWeight { get; set; }

		private Dictionary<string, int> Cache { get; set; }

		public BoundedKnapsackTopDown()
		{
			Cache = new Dictionary<string, int>();
			Items = new List<ZeroOneItem>();
			MaxWeight = 0;
		}

		private string CacheKey(int index, int weight)
		{
			return $"{index}/{weight}";
		}

		private int Memo(int index, int weight)
		{
			Console.WriteLine($"{index}/{weight}");
			if (index < 0) {
				Console.WriteLine("d");
				return 0;
			}
			if (weight <= 0) {
				Console.WriteLine("e");
				return 0;
			}
			if (Cache.ContainsKey(CacheKey(index, weight))) {
				return Cache[CacheKey(index, weight)];
			}
			if (Items[index].Weight > weight) {
				Console.WriteLine("a");
				return Memo(index - 1, weight);
			}
			Console.WriteLine("b");
			var newItem = Math.Max(Items[index].Value + Memo(index - 1, weight - Items[index].Weight), Memo(index - 1, weight));
			Console.WriteLine("c");
			Cache[CacheKey(index, weight)] = newItem;
			Console.WriteLine($"A{index}/{weight}=${newItem}");
			return newItem;
		}

		public int Solve()
		{
			if (MaxWeight == 0) {
				return 0;
			}
			var zeroWeightItems = Items.Where(item => item.Weight == 0);
			if (zeroWeightItems.Any(item => item.Value > 0)) {
				return int.MaxValue;
			}
			Cache[CacheKey(0, MaxWeight)] = 0;
			return Memo(Items.Count() - 1, MaxWeight);
		}
	}
}
