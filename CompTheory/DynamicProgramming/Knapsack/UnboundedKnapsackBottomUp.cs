﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompTheory.DynamicProgramming.Knapsack
{
	public class UnboundedKnapsackBottomUp
	{
		public List<WeightValueItem> Items { get; set; }

		public int MaxWeight { get; set; }

		public UnboundedKnapsackBottomUp()
		{
			Items = new List<WeightValueItem>();
			MaxWeight = 0;
		}

		public int Solve()
		{
			if (MaxWeight == 0) {
				return 0;
			}
			var zeroWeightItems = Items.Where(item => item.Weight == 0);
			if (zeroWeightItems.Any(item => item.Value > 0)) {
				return int.MaxValue;
			}
			Items.Sort((a, b) => (b.Value / b.Weight).CompareTo(a.Value / a.Weight));
			//cake_tuples = [(7, 160, 3.5), (3, 90, 5), (2, 15, 1.5)]
			//capacity = 20
			var weight = MaxWeight;
			var value = 0;
			var i = 0;
			while (i < Items.Count()) {
				if (weight == 0) {
					break;
				}
				var num = (int)Math.Truncate((decimal)weight / Items[i].Weight);
				Console.WriteLine($"{i} += {num}");
				if (num == 0) {
					i += 1;
					continue;
				}
				value += num * Items[i].Value;
				weight -= num * Items[i].Weight;
				i += 1;
			}
			return value;
		}
	}
}
