﻿using System;
namespace CompTheory.DynamicProgramming.Knapsack
{
	public class ZeroOneItem
	{
		public int Value { get; set; }
		public int Weight { get; set; }
		private bool x;
		public int X {
			get { return x ? 1 : 0; }
			set { this.x = value == 1; }
		}

		public ZeroOneItem()
		{
			x = false;
		}
	}
}
