﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompTheory.DynamicProgramming.Knapsack
{
	public class UnboundedKnapsackTopDown
	{
		public List<WeightValueItem> Items { get; set; }

		public int MaxWeight { get; set; }

		public Dictionary<int, int> Cache { get; set; }

		public UnboundedKnapsackTopDown()
		{
			Cache = new Dictionary<int, int> {
				[0] = 0
			};
			Items = new List<WeightValueItem>();
			MaxWeight = 0;
		}

		private int Memo(int weight)
		{
			if (weight <= 0) {
				return 0;
			}
			if (Cache.ContainsKey(weight)) {
				return Cache[weight];
			}
			var items2 = Items
				.Where(item => item.Weight <= weight);
			if (!items2.Any()) {
				return 0;
			}
			var newCache = items2
				.Max(item => item.Value + Memo(weight - item.Weight));
			Cache[weight] = newCache;
			return newCache;
		}

		public int Solve()
		{
			if (MaxWeight == 0) {
				return 0;
			}
			var zeroWeightItems = Items.Where(item => item.Weight == 0);
			if (zeroWeightItems.Any(item => item.Value > 0)) {
				return int.MaxValue;
			}
			return Memo(MaxWeight);
		}
	}
}
