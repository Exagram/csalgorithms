﻿using System;
namespace CompTheory.DynamicProgramming.Knapsack
{
	public class WeightValueItem
	{
		public int Weight { get; set; }

		public int Value { get; set; }
	}
}
