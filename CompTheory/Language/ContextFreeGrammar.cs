﻿using System;
using System.Collections.Generic;

namespace CompTheory
{
	public class ContextFreeGrammar : FormalGrammar<ContextFreeProductionRule>
	{
		public ContextFreeGrammar ()
		{
		}
	}

	public class ContextFreeProductionRule {
		public Symbol Key { get; set; }
		public IList<Symbol> Value { get; set; }
	}
}

