﻿using System;
using System.Collections.Generic;

namespace CompTheory
{
	public class RegularGrammar 
		: FormalGrammar<RegularProductionRule>
	{
		public RegularGrammar ()
		{
		}
	}

	public class RegularProductionRule {
		public NonTerminalSymbol Key { get; set; }
		public TerminalSymbol Value1 { get; set; }
		public IList<Symbol> Value2 { get; set; }
		public bool IsLeft { get; set; } = false;
	}
}

