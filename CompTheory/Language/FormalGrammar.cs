﻿using System;
using System.Collections.Generic;

namespace CompTheory
{
	/// <summary>
	/// Formal grammar.
	/// https://en.wikipedia.org/wiki/Formal_grammar
	/// </summary>
	public class FormalGrammar<IProductionRule>
	{
		public ISet<NonTerminalSymbol> NonTerminableSymbols { get;set; }
		public ISet<TerminalSymbol> TerminableSymbols { get;set; }
		public ISet<IProductionRule> ProductionRules { get; set; }

		public FormalGrammar ()
		{
		}

		public ISet<IProductionRule> ProductionRule { get;set; }
	}
}

