﻿using System;
namespace CompTheory.LinearAlgebra
{
	public struct Vector2<T>
	{
		public T X { get; set; }
		public T Y { get; set; }

		public Vector2(T x, T y)
		{
			X = x;
			Y = y;
		}

		public override string ToString()
		{
			return $"({X}, {Y})";
		}
	}
}
