﻿using System;

namespace CompTheory {
	public class Node<T> {

		T t;

		public Node() {
			t = default(T);
		}

		public Node(T t) : base() {
			withValue (t);
		}

		public T getValue() {
			return t;
		}

		public Node<T> withValue(T t) {
			this.t = t;
			return this;
		}
	}
}

