using System;
using System.Collections.Generic;
using System.Linq;

namespace CompTheory {
	public class Utils {
		public static void toSet<T>(IEnumerable<T> list, ISet<T> result) {
			foreach (var elem in list) {
				result.Add (elem);
			}
		}
	}
}

