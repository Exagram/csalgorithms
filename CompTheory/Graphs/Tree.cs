using System;
using System.Collections.Generic;
using System.Linq;

namespace CompTheory {

	public class TreeNode<NodeType> : Node<NodeType> {
		public List<TreeNode<NodeType>> children;

		public TreeNode(NodeType t, List<TreeNode<NodeType>> children) {
			withValue (t);
			this.children = children;
		}
	}

	public class Tree<NodeType> {

		public TreeNode<NodeType> root;

		public Tree () {
		}

		public Tree (TreeNode<NodeType> root) {
			this.root = root;
		}
	}
}

