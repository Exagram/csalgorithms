﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace CompTheory {

	public enum VisitState {
		unvisited,
		visited
	}

	public class FFEdge<NodeData> : Edge<FFNode<NodeData>>, ICloneable {
		public int maxcap;
		public int flow;

		public FFEdge() : base() {
			nodes.Capacity = 2;
			maxcap = 0;
			flow = 0;
		}

		public FFEdge(FFNode<NodeData> _from, FFNode<NodeData> to, int maxcap) : base(_from, to) {
			this.maxcap = maxcap;
			flow = 0;
		}

		public int getIncrementCapacity(FFNode<NodeData> node) {
			if (From.Equals(node)) {
				return flow;
			}

			return maxcap - flow;
		}

		public void addIncrementFlow(FFNode<NodeData> node, int amount) {
			if (From.Equals(node)) {
				flow -= amount;
			} else if (To.Equals(node)) {
				flow += amount;
			}
		}

		//public virtual FFNode<NodeData> opposite(FFNode<NodeData> node) {
		//	if (From.Equals(node)) {
		//		return To;
		//	}
		//
		//	return From;
		//}

		#region ICloneable implementation
		public object Clone () {
			var o = new FFEdge<NodeData> (From, To, maxcap);
			o.flow = flow;
			return o;
		}
		#endregion
	}

	public class FFNode<NodeData> : Node<NodeData>, ICloneable {

		public FFNode(NodeData v) : base(v) {
		}

		#region ICloneable implementation
		public object Clone () {
			var o = new FFNode<NodeData> (getValue ());
			return o;
		}
		#endregion
		
	}

	public class FlowNetwork<NodeData> : Graph<FFNode<NodeData>, FFEdge<NodeData>> {

		public FlowNetwork(ISet<FFEdge<NodeData>> e, ISet<FFNode<NodeData>> n, FFNode<NodeData> source, FFNode<NodeData> sink) : base() {
			this.edges = e;
			this.nodes = n;
			Source = source;
			Sink = sink;
		}

		public FlowNetwork<NodeData> getExpandedNetwork() {
			var newEdges = new HashSet<FFEdge<NodeData>> ();
			edges
				.ToList ()
				.Select (edge => (FFEdge<NodeData>)edge.Clone ())
				.ToList ()
				.ForEach (x => newEdges.Add(x));
			
			var g = new FlowNetwork<NodeData> (newEdges, nodes, Source, Sink);
			g.edges.Add(new FFEdge<NodeData>(Sink, Source, int.MaxValue));
			return g;
		}

		public int getFlowValue() {
			int flow = 0;
			foreach (var edge in edges.Where(x => x.From == Sink)) {
				flow -= edge.flow;
			}
			foreach (var edge in edges.Where(x => x.To == Sink)) {
				flow += edge.flow;
			}
			return flow;
		}

		public void bfs() {
			var queue = new Queue<FFNode<NodeData>> ();
			queue.Enqueue (Source);
			Console.WriteLine ("Node: " + Source.getValue ());

			var visited = nodes.ToDictionary (x => x, x => VisitState.unvisited);
			visited [Source] = VisitState.visited;

			while (queue.Count > 0) {
				var node = queue.Dequeue ();
				Console.WriteLine ("Node: " + node.getValue ());

				FFNode<NodeData> child = null;
				while ((child = edges
				        .Where(x => x.From == node)
				        .Select(x => x.To)
				        .FirstOrDefault(y => visited[y] == VisitState.unvisited)) != null) {
					visited [child] = VisitState.visited;
					queue.Enqueue (child);
				}
			}
		}

		public FFNode<NodeData> Source {
			get;
			set;
		}

		public FFNode<NodeData> Sink {
			get;
			set;
		}

		public override string print() {
			return string.Join("\n", edges.ToList().Select(edge => {
				return edge.From.getValue() + " >>-- " + edge.flow + "/" + edge.maxcap + " -->> " + edge.To.getValue();
			}));
		}
	}

	/// <summary>
	/// Ford-Fulkerson Algorithm.
	/// 
	/// Augmented paths with:
	/// BFS: Edmonds-Karp --> O(|V| * |E|^2)
	/// DFS: Dinic --> O(|V|^2 * |E|)
	/// </summary>
	public class MaxFlow<NodeData> {
		public Dictionary<FFNode<NodeData>, FFEdge<NodeData>> flowEdges;
		public int FlowValue;
		public FlowNetwork<NodeData> graph;

		public MaxFlow(FlowNetwork<NodeData> graph) {
			this.graph = graph;
			findMaxFlow (graph);
		}

		public void findMaxFlow(FlowNetwork<NodeData> graph) {

			//Step 1) All Flows = 0
			graph.edges.ToList ().ForEach (edge => edge.flow = 0);

			flowEdges = new Dictionary<FFNode<NodeData>, FFEdge<NodeData>>();
			FlowValue = 0;
			var delta = int.MaxValue;

			//Step 2) While there is a path "p" from s->t in G_f s.t. c_f(u,v) > 0 for all edges (u,v) \in "p":
			while (hasAugmentingPath(graph)) {
				for (var v = graph.Sink; v != graph.Source; v = flowEdges[v].opposite(v)) {
					delta = Math.Min (delta, flowEdges [v].getIncrementCapacity (v));
				}

				for (var v = graph.Sink; v != graph.Source; v = flowEdges[v].opposite(v)) {
					flowEdges [v].addIncrementFlow (v, delta);
				}

				FlowValue += delta;
			}
		}

		public bool hasAugmentingPath(FlowNetwork<NodeData> graph) {
			var queue = new Queue<FFNode<NodeData>> ();
			queue.Enqueue (graph.Source);

			var visited = graph.nodes.ToDictionary (x => x, x => VisitState.unvisited);
			visited [graph.Source] = VisitState.visited;

			while (queue.Count > 0) {
				var node = queue.Dequeue ();

				foreach(var edge in graph.edges.Where(x => x.From == node || x.To == node)) {
					var otherNode = edge.opposite (node);

					if (edge.getIncrementCapacity (otherNode) > 0 && visited [otherNode] == VisitState.unvisited) {
						visited [otherNode] = VisitState.visited;
						flowEdges[otherNode] = edge;
						queue.Enqueue (otherNode);
					}
				}
			}

			return visited [graph.Sink] == VisitState.visited;
		}
	}
}

