using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace CompTheory {

	public class DijkstraNode<NodeType> : Node<NodeType>, ICloneable {
		public int distance;

		public DijkstraNode() : base() {
		}

		public DijkstraNode(NodeType value) {
			withValue (value);
		}

		#region ICloneable implementation

		public object Clone () {
			var result = new DijkstraNode<NodeType> ();
			result.withValue (getValue ());
			result.distance = this.distance;
			return result;
		}

		#endregion
	}

	public class DijkstraEdge<NodeType> : Edge<DijkstraNode<NodeType>> {
		public int cost;

		public DijkstraEdge() : this(null, null, 0) {
		}

		public DijkstraEdge(DijkstraNode<NodeType> @from, DijkstraNode<NodeType> to, int cost) : base(@from, to) {
			this.cost = cost;
		}
	}

	public class DijkstraGraph<NodeData> : Graph<DijkstraNode<NodeData>, DijkstraEdge<NodeData>> {

		public DijkstraGraph(ISet<DijkstraNode<NodeData>> nodes, ISet<DijkstraEdge<NodeData>> edges) : base(nodes, edges) {

		}

		public IEnumerable<DijkstraNode<NodeData>> shortestPath(DijkstraNode<NodeData> start, DijkstraNode<NodeData> target) {

			ISet<DijkstraNode<NodeData>> vset = new HashSet<DijkstraNode<NodeData>> ();
			nodes
				.ToList ()
				.ForEach (x => {
					x.distance = int.MaxValue;
					vset.Add (x);
				});
			start.distance = 0;

			var previous = new Dictionary<DijkstraNode<NodeData>, DijkstraNode<NodeData>> ();
			previous [start] = null;

			while (vset.Count > 0) {
				var shortestDistance = vset.Min (x => x.distance);
				var shortestNode = vset.FirstOrDefault (x => x.distance == shortestDistance);
				vset.Remove (shortestNode);
				if (shortestNode == target) {
					break;
				}

				edges
					.Where(edge => edge.From == shortestNode || edge.To == shortestNode)
					.ToList ()
						.ForEach (edge => {
							DijkstraNode<NodeData> neighbor = null;
							if(vset.Contains(edge.opposite(shortestNode))) {
								neighbor = edge.opposite(shortestNode);
							}
							if(neighbor == null) {
								return;
							}

							var alt = shortestNode.distance + edge.cost;
							if(alt < neighbor.distance) {
								neighbor.distance = alt;
								previous [neighbor] = shortestNode;
							}
						});
			}

			var result = new List<DijkstraNode<NodeData>> ();
			DijkstraNode<NodeData> u = target;
			while(previous[u] != null) {
				result.Add (u);
				u = previous [u];
			}
			result.Add (start);
			result.Reverse (0, result.Count);
			return result;
		}
	}
}
