﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompTheory.LinearAlgebra;

namespace CompTheory.Graphs
{
    public class ConvexHull
    {
		public List<Vector2<int>> Input { get; set; }

		public void Run()
		{
			Input.Sort((x, y) => x.X.CompareTo(y.X));
			List<Vector2<int>> result = new List<Vector2<int>>();
			//1,0
			//1,1
			//1,2
			var dict1 = new Dictionary<int, Vector2<int>>();
			for (int i = 0; i < Input.Count; i++) {
				var input = Input[i];
				if (!dict1.ContainsKey(input.X)) {
					dict1.Add(input.X, input);
				} else if (input.Y < dict1[input.X].Y) {
					dict1[input.X] = input;
				}
			}
			foreach (KeyValuePair<int, Vector2<int>> entry in dict1) {
				result.Add(entry.Value);
				Console.WriteLine(entry.Value);
			}
		}
    }
}
