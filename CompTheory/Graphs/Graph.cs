﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompTheory {
	public class Graph<NodeType, EdgeType> {
		
		public ISet <EdgeType> edges;
		public ISet <NodeType> nodes;

		public Graph() {}

		public Graph(ISet<NodeType> nodes, ISet<EdgeType> edges) : this() {
			this.edges = edges;
			this.nodes = nodes;
		}

		public virtual string print() {
			return "?";
		}
	}
}

