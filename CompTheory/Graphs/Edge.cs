﻿using System;
using System.Collections.Generic;

namespace CompTheory {
	public class Edge<NodeType> {

		public static Edge<NodeType> zero = new Edge<NodeType>();

		public List<NodeType> nodes;
		public bool isDir;

		public Edge() {
			nodes = new List<NodeType>();
			isDir = false;
		}

		public Edge(NodeType _from, NodeType to) : this() {
			nodes.Add (_from);
			nodes.Add (to);
			isDir = true;
		}

		public NodeType From {
			get {
				return nodes[0];
			}
			set {
				nodes[0] = value;
			}
		}

		public NodeType To {
			get {
				return nodes[1];
			}
			set {
				nodes[1] = value;
			}
		}

		public NodeType opposite(NodeType node) {
			if (From.Equals(node)) {
				return To;
			}

			return From;
		}
	}
}

